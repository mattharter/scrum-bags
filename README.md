
OSX:

1. Brew install python
2. sudo pip3 install --upgrade pip
3. sudo pip3 install -U Flask
4. sudo pip3 install -U flask-cors
5. sudo pip3 install simplejson
6. sudo pip3 install icrawler

Windows:

2. update PATH system environment variable.  append: C:\ {your python folder}
3. update PATH system environment variable.  append: C:\ {your python folder}\Scripts
4. cmd prompt: pip3 install --upgrade pip
5. cmd prompt: pip3 install -U Flask
6. cmd prompt: pip3 install -U flask-cors
7. cmd prompt: pip3 install simplejson


DirectoryTraversal.py
Send an absolute file path as a string
    Ex: /users/username/directory
    EX: /users/username/directory/subdirectory/etc

Links to documentation files:
User documentation:  https://docs.google.com/a/anderson.edu/document/d/1lHSt62mqWTIgKkbpr-aLOiH574I35wmwvVbSBvuMsnY/edit?usp=sharing
Installation Instructions: https://docs.google.com/a/anderson.edu/document/d/1sb9uSW8Z2_aGawA7PvJl8ynGfAxprrqgEQz_lN9sIOQ/edit?usp=sharing
User Stories (Commented for status): https://docs.google.com/a/anderson.edu/document/d/18E1Nte6W4_hFZcCUZgHJz3HCqMYCHDlTOjbIcpLVmLQ/edit?usp=sharing
