function viewModel() {
    //ko convention to help with scope on multiple view models
    var self = this;

    self.uri = 'http://localhost:5000/';

    //variables and arrays that are bounded to html tags to give a live reload
    self.pictures = ko.observableArray();
    self.isPictures = ko.observable(false);
    self.healthCheck = ko.observable();
    self.searchOpened = ko.observable(true);
    self.selectedPictures = ko.observableArray();
    self.previewPicture = ko.observable('');
    self.reportView = ko.observable(false);
    self.previewLat = ko.observable('');
    self.previewLong = ko.observable('');
    self.previewDate = ko.observable('');
    self.aboutModalOpen = ko.observable(false);
    self.dbModalOpen = ko.observable(false);
    self.directory = ko.observable(null);

    function myFunction() {
        alert("All Selected Images Have been marked in the database!");
    };

    //sets the observable objects for the preview window
    self.previewImage = function(img) {
        self.previewPicture(img.file);
        self.previewLat(img.Latitude);
        self.previewLong(img.Longitude);
        self.previewDate(img.DateTime);
    };

    //monitors the healthcheck observable and returns it to the ui
    self.displayHealthCheck = ko.computed(function(){
        console.log("Returning updated healthCheck to ui");
        return self.healthCheck();
    });

    //updates the selected pictures array
    self.selectPicture = function (picture) {

        var wasSelected = false;    //bool flag

        //for each loop on the selected picture array to either add or removed depending on whether it is selected or not
        ko.utils.arrayForEach(self.selectedPictures(), function(selectedPicture) {

            //remove from array if already selected as inappropriate
            if(selectedPicture.file == picture.file){
                console.log('unmarking picture' + selectedPicture.file);
                self.selectedPictures.remove(picture);
                wasSelected = true;
            }
        });

        //if the picture was not in the array then add it
        if(!wasSelected) {
            console.log('marking picture: ' + picture.file)
            self.selectedPictures.push(picture);
        }

        return true;
    };

    //checks if picture is selected or not for css property
    self.isSelected = function(img) {
        var flag = false;

        ko.utils.arrayForEach(self.selectedPictures(), function(picture) {
            if(img.file == picture.file) {
                flag = true;
            }
        });

        return flag;
    };

    //opens up the health check modal
    self.displayHealthCheckModal = function () {
        self.aboutModalOpen(true);
        console.log("Displaying healthCheck modal");
        self.getHealthCheck();
        $('#aboutModal').modal('show');
        return true;
    };

    //opens up the database diagram
    self.displayDiagram = function () {
        self.dbModalOpen(true)
        $('#dbModal').modal('show');
        return true;
    };

    //closes whatever modal is open currently
    self.closeModal = function(modal) {
        modal(false);
    };

    //moves the side bar back and forth
    self.toggleSlideout = function () {
        if(self.searchOpened()) {
            $('.sidebar-main').animate({left: '-15%'}, 'slow', function () {});
            $('.preview-main').animate({left: '0'}, 'slow', function(){});
        } else {
            $('.sidebar-main').animate({left: '0%'}, 'slow', function () {});
            $('.preview-main').animate({left: '15%'}, 'slow', function(){});
        }

        self.searchOpened(!self.searchOpened());
    };

    /****** API CALLS ******/

    //Updates the database with suspicious images
    self.putToDatabase = function () {

        var param = '';

        //for each loop that concatenates the file paths into a comma separated string
        ko.utils.arrayForEach(self.selectedPictures(), function(img) {
            param += img.file + ','
        })

        param = param.substr(0, param.length - 1)
        $.ajax({
           type:'PUT',  //type of api call
           url: self.uri + 'database/' + '?img=' + param, //endpoint in service
           dataType: "json",    //data being returned

           //upon success of api call (200)
           success: function(data) {
                   console.log('write success');
               },
           //upon failure of api call
           error: function(jq, st, error) {
               console.log(error);
               console.log(st);
               console.log(jq);
               alert(error);
           }
        })
        myFunction()
    };

    //generates a report of inappropriate images from the database
    self.generateReport = function() {
         $.ajax({
            type:'GET',
            url: self.uri + 'Report/',
            dataType: "json",

            success: function(data) {
                    console.log('Report Generated');
                    window.location.assign('../database/Susp_Img_Report.csv');
                },

            error: function(jq, st, error) {
                console.log(error);
                console.log(st);
                console.log(jq);
                alert(error);
            }
        })
    };

    //finds all of the pictures from the selected directory
    self.findPictures = function () {

        //this sets up the service to return a 404 if there is no selected directory
        var url = 'ImageExif/';
        console.log(self.directory());
        if(self.directory() != null){
            url = self.uri + 'ImageExif/?dir=' + self.directory()
        }
        $.ajax({
            type:'GET',
            url: url,
            dataType: "json",

            //data is an array of the objects data returned from traversal
            success: function(data) {
                    console.log('received data');
                    self.pictures(data);
                    self.isPictures(true); //bool flag updates the html
                },

            error: function(jq, st, error) {
                console.log(jq);
                console.log(st);
                console.log(error);
                alert("Please select a directory"); //prompt user to select a directory
            }
        })
    }

    //ajax call to health check api (this doesn't do anything really)
    self.getHealthCheck = function () {
        $.ajax({
            type: 'GET',
            url: self.uri + 'health',
            dataType: "json",
            success: function(data) {
                console.log(JSON.stringify(data));
                self.healthCheck({service: data.service, version: data.version})
            },
            error:function(jq, st, error){
                alert(error);
            }
        });
    };

    /****** END OF API CALLS ******/

}


//loads the viewModel as soon as the page is ready
$(document).ready(function () {
    console.log("Loading the viewmodel");
    ko.applyBindings(new viewModel());
});
