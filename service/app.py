"""@package app.py
Main python service module. Used to launch service that
handles requests made by the UI
"""
from flask import Flask, request, jsonify, send_file
from flask_cors import CORS
import DirectoryTraversal
import os
import sys
import sqlite3
import csv

version = "1.0"
name = "The Scrum Bags"
DEBUG = True

app = Flask(__name__)
app.config.from_object(__name__)
CORS(app)


# health check. returns simple info and confirms that service is up
@app.route('/health', methods=["GET"])
def gethealthcheck():
    """Function "gethealthcheck" checks to make sure that
    the service is still alive."""
    return jsonify({"service": name, "version": version})


@app.route('/goats/', methods=["GET"])
def getGoats():
    """Function "getGoats" gets the goats."""

    global array
    array = []
    for fileName in os.listdir("../mountain_goat"):
        if fileName[-3:] == 'jpg':
            print(fileName)
            # find the file and get its exif data
            path = "../mountain_goat" + '/' + fileName
            array.append(path)
    print(array)
    return jsonify({"dir": array}), 200

# magic that fixes everything
@app.route('/goat/', methods=["GET"])
def getGoat():
    """Function "getGoats" gets the goats."""

    global array
    array = []
    for fileName in os.listdir("../mountain_goat"):
        if fileName[-3:] == 'jpg':
            print(fileName)
            # find the file and get its exif data
            path = "../mountain_goat" + '/' + fileName
            array.append(path)
    return send_file(array[0], mimetype='image/jpeg')
    print(array)


@app.route('/ImageExif/', methods=["GET"])
def findImages():
    """Function "findImages" gets images and exif data in a JSON format."""
    directory = str(request.args.get("dir")) if request.args.get("dir") is not None else False
    if directory:
        image_dictionary = DirectoryTraversal.get_exif(directory)

        os.chdir(sys.path[0])  # Set current dir as working dir (Derek workaround)

        device_insertsql = "INSERT INTO DEVICES VALUES (null, null, null, null);"
        db_connection = sqlite3.connect("../database/sb_forensic_images.db")  # Establish db connection
        db_connection.execute(device_insertsql)  # Execute SQL against DB to insert new device row

        device_id = db_connection.execute(
            "SELECT MAX(DEVICE_ID) FROM DEVICES;").fetchone()  # Get highest device_id (current device)
        device_id = device_id[0]  # Strip away extra returned data to get only device_id

        for image_data in image_dictionary:
            img_insert_sql = 'INSERT INTO IMAGES VALUES (null, "' + str(device_id) + '", "' + str(
                image_data["Name"]) + '", "' + str(image_data["Extention"]) + '", "' + str(
                image_data["file"]) + '", "' + str(image_data["DateTime"]) + '", "' + str(
                image_data["Latitude"]) + '", "' + str(image_data["Longitude"]) + '", 0);'
            db_connection.execute(img_insert_sql)  # Insert image into database

        db_connection.commit()
        db_connection.close()

        return jsonify(image_dictionary), 200
    else:
        return jsonify({'response': 'directory not found'}), 404


@app.route('/database/', methods=["PUT"])
def putImages():
    """Function "putImages" gets and updates the images
    tagged as suspicious by the user and updates the
    database flag IMAGE_SUSP_FLAG to 1 for such images"""

    # request gets the arguments from the url
    bad_imgs = str(request.args.get("img")) if request.args.get("img") is not None else False

    img_list = []
    if bad_imgs:
        img_list = bad_imgs.split(',')

    print(img_list)

    db_connection = sqlite3.connect("../database/sb_forensic_images.db")  # Establish connection to database located in the specified path

    for file_path in img_list:
        print('Writing ' + file_path + ' to database')
        sql_statement = ''
        sql_statement = "UPDATE IMAGES SET IMAGE_SUSP_FLAG = 1 WHERE IMAGE_PATH = " + '"' + file_path + '"' + ';'
        db_connection.execute(sql_statement)  # Execute SQL against DB and load into data object
        
    db_connection.commit()
    db_connection.close()

    return jsonify({'success': True}), 200


@app.route('/Report/', methods=["GET"])
def export_csv():
    """Function "export_csv" establishes a connection to the database,
    executes a SQL query that satisfies user story #7, and exports the
    necessary data as an easily readable CSV file"""

    csv_export_sql = "SELECT IMAGE_NAME, IMAGE_PATH, IMAGE_DATETIME, IMAGE_GPS_LAT, IMAGE_GPS_LONG FROM IMAGES WHERE IMAGE_SUSP_FLAG = 1 and DEVICE_ID = (SELECT MAX(DEVICE_ID) FROM DEVICES)"  # SQL query to extract the suspicious images for current device
    db_connection = sqlite3.connect("../database/sb_forensic_images.db")  # Establish connection to database
    data = db_connection.execute(csv_export_sql)  # Execute SQL against DB and load into data object

    # Write the results of the query to a CSV file
    with open("../database/Susp_Img_Report.csv", 'w') as csv_out:  # Write csv file to database folder
        writer = csv.writer(csv_out)
        writer.writerow([i[0] for i in data.description])  # Write column headers first
        writer.writerows(data)  # Write the data

    db_connection.close()
    return jsonify({'success': True}), 200

"""Prolly won't use it, but keeping as a just in case."""
# @app.route('/filepath/', methods=["GET"])
# def give_a_filepath():
#     filepath = str(request.args.get("absoluteFilePath")) if request.args.get("absoluteFilePath") is not None else False
#     filetype = "image/"
#     if filepath[-3:] == 'jpg' | 'peg':  # Finds out if the file is a jpeg file
#         filetype = filetype + 'jpeg'
#     elif filepath[-3:] == 'png':
#         filetype += 'png'
#     elif filepath[-3:] == 'gif':
#         filetype[-3:] == 'gif'
#     elif filepath[-3:] == 'bmp':
#         filepath[-3:] =='bmp'
#     """Function "getGoats" gets the goats."""
#     return send_file(filepath, mimetype=filetype), 200


# Runs this as a service
if __name__ == '__main__':
    app.run(host="0.0.0.0", threaded=True)
