import os  # Used for validating if directory exists.
import PIL  # Used for accessing exif data in images.
from PIL import Image, ExifTags  # Used for accessing images and exif tags
from PIL.ExifTags import TAGS, GPSTAGS  # Used for accessing tags for gps data.


## finds the gps exif data
#  @param image - image to get gps exif data from
#  @return exif_data - gps exif data
def find_gps_data(image):
    """Returns a dictionary from the exif data of an PIL Image item. Also converts the GPS Tags"""

    exif_data = {}  # Empty dictionary.
    info = image._getexif()  # Getting exif info from 'image'.
    if info:  # If there is any exif data begin extracting gps data.
        for tag, value in info.items():  # Loops through all exif tags in info and grabs values.
            decoded = TAGS.get(tag, tag)  # Gets the tags.
            if decoded == "GPSInfo":  # Verifing that 'decoded' is 'GPSInfo'.
                gps_data = {}  # Empty dictionary.
                for gps_tag in value:  # Loops through value to get latitude and longitude.
                    sub_decoded = GPSTAGS.get(gps_tag, gps_tag)
                    gps_data[sub_decoded] = value[gps_tag]  # Adding value into 'gps_data' dictionary.

                exif_data[decoded] = gps_data
            else:
                exif_data[decoded] = value

    return exif_data


## checks if exif data exists
#  @param data - exif data
#  @param key - hash key
#  @return data[key] - exif data
def _get_if_exist(data, key):
    """Checks if exif data exists"""
    if key in data:
        return data[key]

    return None


## converts gps data to degrees
#  @param value - gps value
#  @return gps data in degrees
def _convert_to_degrees(value):
    """Helper function to convert the GPS coordinates stored in the EXIF to degrees in float format"""
    d0 = value[0][0]
    d1 = value[0][1]
    d = float(d0) / float(d1)

    m0 = value[1][0]
    m1 = value[1][1]
    m = float(m0) / float(m1)

    s0 = value[2][0]
    s1 = value[2][1]
    s = float(s0) / float(s1)

    return d + (m / 60.0) + (s / 3600.0)  # Returns converted GPS coordinates in degrees.


## Returns the latitude and longitude, if available, from the provided exif_data (obtained through get_exif_data above)
#  @param exif_data - gps exif data in degrees
#  @return lat, lon - latitude and longitude values
def get_lat_lon(exif_data):
    """Returns the latitude and longitude, if available, from the provided exif_data (obtained through get_exif_data above)"""
    lat = None
    lon = None

    if "GPSInfo" in exif_data:  # If GPSInfo is in 'exif_data' dictionary then get latitute and longitude.
        gps_info = exif_data["GPSInfo"]

        gps_latitude = _get_if_exist(gps_info, "GPSLatitude")  # Calls function '_get_if_exist(x, "y")'.
        gps_latitude_ref = _get_if_exist(gps_info, 'GPSLatitudeRef')  # Calls function '_get_if_exist(x, "y")'.
        gps_longitude = _get_if_exist(gps_info, 'GPSLongitude')  # Calls function '_get_if_exist(x, "y")'.
        gps_longitude_ref = _get_if_exist(gps_info, 'GPSLongitudeRef')  # Calls function '_get_if_exist(x, "y")'.

        if gps_latitude and gps_latitude_ref and gps_longitude and gps_longitude_ref:  # Calls 'convert_to_degress' function.
            lat = _convert_to_degrees(gps_latitude)
            if gps_latitude_ref != "N":
                lat = 0 - lat

            lon = _convert_to_degrees(gps_longitude)
            if gps_longitude_ref != "E":
                lon = 0 - lon

    return lat, lon  # Returning values for latitude and longitude.


## Gets the latitude
#  @param lat_lon_tuple - tuple with latitude and longitude
#  @return lat_lon_tuple[0] - latitude
def get_latitude(lat_lon_tuple):
    return lat_lon_tuple[0]


## Gets the longitude
#  @param lat_lon_tuple - tuple with latitude and longitude
#  @return lat_lon_tuple[1] - longitude
def get_longitude(lat_lon_tuple):
    return lat_lon_tuple[1]


## gets the gps, datetime, name, extension, and file path of images in a directory
#  @param dir_root - starting directory
#  @param img_dict - dictionary to hold image data
#  @return new_dict - dictionary of dictionaries containing image exif data
def get_exif(dir_root):
    """Gets the exif data specifically the DateTime"""

    return_list = []

    for root, dirs, files in os.walk(dir_root, topdown=False):  # Looping through directory to get image files.
        for name in files:  # Looping through files for correct extension.
            img_dict = {}
            ext = [".jpg", ".JPG", ".png", ".PNG", ".jpg", ".jpeg", ".JPEG", ".bmp", ".BMP", ".gif", ".GIF"]
            try:
                if name.endswith(tuple(ext)):

                    img = PIL.Image.open(root + '/' + name)

                    try:  # Ensuring no AttributeErrors occur during search for exif data.
                        exif = {PIL.ExifTags.TAGS[k]: v
                            for k, v in img._getexif().items()
                            if k in PIL.ExifTags.TAGS}

                        if exif is not None:  # If there is dateTime exif data get the data and put in a dictionary
                            if 'DateTimeOriginal' in exif.keys():
                                img_dict["DateTime"] = str(exif['DateTimeOriginal'])
                            elif 'DateTime' in exif.keys():
                                img_dict["DateTime"] = str(exif['DateTime'])
                            else:
                                img_dict["DateTime"] = None

                        else:
                            img_dict["DateTime"] = None

                    except AttributeError as e:  # If there is an AttributeError then say no exif data.
                        img_dict["DateTime"] = None

                    try:  # Ensuring no AttributeErrors occur during search for GPS data.
                        exif_data = find_gps_data(img)
                        img_dict["Latitude"] = str(get_latitude(get_lat_lon(exif_data)))
                        img_dict["Longitude"] = str(get_longitude(get_lat_lon(exif_data)))

                    except AttributeError as e:  # If there is an AttributeError then say no GPS data.
                        img_dict["Latitude"] = None
                        img_dict["Longitude"] = None

                    img_dict["Name"] = name  # puts the name of the image into the dictionary
                    extention = os.path.splitext(name)  # gets the extention of the image
                    img_dict["Extention"] = extention[1]  # puts the extention into the dictionary

                    file_path = root + "/" + name  # gets the file path of the image
                    img_dict['file'] = file_path
                    return_list.append(img_dict)  # puts the file path as the key for the dictionary
            except Exception as e:
                print('Corrupt File: ')
                print(e)
    return return_list

def main():
    """Calls primary functions to get images and exif data."""

    image_dictionary = {}

    dir_root = input('Enter root directory:')  # User input.

    image_dictionary = get_exif(dir_root, image_dictionary)  # Calling function 'get_exif'.

    print(image_dictionary)


# Calling function 'main'.
if __name__ == '__main__':
    main()
